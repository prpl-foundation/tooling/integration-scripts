#!/usr/bin/env python3
import requests, sys, datetime

headers={}
PRPL_GROUP_ID=6326218
RESULTS_PER_REQUEST=100 # 100 is the maximum, and probably the most efficient

buckets=['components', 'prplos', 'lcm', 'prplrdkb', 'prplmesh']
commit_stats={}
csv_filename = 'gitlab_results.csv'


def gitlab_request(base_uri: str, args: dict):
    args['per_page'] = str(RESULTS_PER_REQUEST) 
    done = False
    page = 1
    response = []
    while not done:
        args['page'] = str(page)
        urlargs = '&'.join('{}={}'.format(k, args[k]) for k in args)
        req=requests.get(base_uri + '?' + urlargs, headers=headers)
        if not req.ok:
            print("Error: %d %s" %(req.status_code, req.reason))
            print(req.text)
            return False, response
        if 'x-next-page' not in req.headers or not req.headers['x-next-page']:
            done = True
        else:
            page = int(req.headers['x-next-page'])
        response += req.json()

    return True, response

def get_all_prpl_projects():
    args = {'simple': 'true', 'include_subgroups': 'true'}

    ok, projects = gitlab_request(f'https://gitlab.com/api/v4/groups/{PRPL_GROUP_ID}/projects', args)
    if not ok:
        print("Unable to get projects of prplfoundation group")
        sys.exit(1)
    return projects

users = {
    5678729:    'bot@softathome.com',
    7862835:    'oscar.leal@vodafone.com',
    14307293:   'i.plesser@inango-systems.com',
    949688:     'ynezz@true.cz',
    13005644:   'saran.vs@vodafone.com',
    9256977:    'jimmy.gysens@softathome.com',
    12141513:   'panchakshari.hs@vodafone.com',
    16987890:   'jeslin.antony@vodafone.com',
    10902286:   'shaji.mathew@vodafone.com',
    12234183:   'Dave.Chapman@consult.red',
    12439119:   'sumitkumar.suman@vodafone.com',
    14653533:   'vponnapalli@maxlinear.com',
    18379919:   'skotapati@maxlinear.com',
    14714358:   'bnbharadiya@maxlinear.com',
    18379512:   'dmurugan@maxlinear.com',
    9634429:    'ahmed-amine.haoues@softathome.com',
    13209671:   'sam.talbot@consult.red',
    13432585:   'nabil.bizid@softathome.com',
    13116879:   'maurizio.belluati@telecomitalia.it',
    13016516:   'emanuele.giacomelli@alten.it',
    6881231:    'sylvain.le-roux@sagemcom.com',
    13308601:   'Richard.Yu@mitrastar.com.tw',
    11266332:   'badhri@maxlinear.com',
    12715215:   'wissem.golli@softathome.com',
    9485025:    'd.puz@inango-systems.com',
    20549671:   'pkarthik@maxlinear.com',
    20464943:   'v.pavlenko@inango-systems.com',
    12309991:   'marouan.mcharfi@softathome.com',
    11614252:   'rramasamy@maxlinear.com',
    15570216:   'd.stolbov@inango-systems.com',
    16992751:   'nicolas.henriques-ext@sagemcom.com',
    13392000:   'a.erokhin@inango-systems.com',
    11618535:   'iacob.juc@softathome.com',
    11230279:   'afif.jebali@softathome.com',
    11706571:   'baptiste.souchier@softathome.com',
    11618861:   'mohammed.siali@softathome.com',
    2665965:    'wouter.cloetens@prplfoundation.org',
    15387619:   'tarek.nakkach_ext@softathome.com',
    18487587:   'ashutosh.shandilya@softathome.com',
    18644974:   'liyun.yang-ext@sagemcom.com',
    15495535:   'kganesh@maxlinear.com',
    13398888:   'mganesh@maxlinear.com',
    13007134:   'bmanoharan@maxlinear.com',
    6682374:    'yyizhak@maxlinear.com',
    9105679:    'jef.marien@softathome.com',
    15020329:   'nchernikov@maxlinear.com',
    15040857:   'rportnikh@maxlinear.com',
    19538960:   'mvuls@maxlinear.com',
    12119106:   'T.Polomik@cablelabs.com',
    15776804:   'sandesh.mahale@vantiva.com',
    12936337:   'Mouna.Aloui_ext@softathome.com',
    12980792:   'yuce.kurum_ext@softathome.com',
    10351059:   'cedric.dourlent@softathome.com',
    13601515:   'CheWei.Chien@wnc.com.tw',
    14714425:   'ukumari@maxlinear.com',
    20125359:   'hoanganh.phan@softathome.com',
    6640368:    'ps@embedd.com',
    20469960:   'mateusz.stepien@consult.red',
    20092553:   'Nataraj.Ramalingam@att.com',
    12686925:   'marvin.lu@wnc.co.tw',
    11428476:   'maarten.dedecker@mind.be',
    16840176:   'kelly.tc.lin@wnc.com.tw',
}

def get_email_from_userid(id: int):
    if id in users:
        return users[id]
    req = requests.get(f'https://gitlab.com/api/v4/users/{id}', headers=headers)
    if not req.ok:
        print("Unable to identify user " + id + "!")
        sys.exit(1)
    user = req.json()
    users[id] = user['public_email']
    if not users[id]:
        print("No email found for", id, user['username'], user['name'])
        users[id] = 'none@anonymous'
    else:
        print("User identified:", id, users[id])
    return users[id]


def get_project_events(pid: int, since: datetime.datetime, until: datetime.datetime):
    args = {'after': since.isoformat(), 'before': until.isoformat()}
    stats = {}

    ok, events = gitlab_request(f'https://gitlab.com/api/v4/projects/{pid}/events', args=args)
    if not ok:
        print("ERROR: Unable to get events of project!")
        return False, None
    for event in events:
        email = get_email_from_userid(event['author_id'])
        if '@' not in email:
            print("Malformed email: no @:", email)
            continue
        company = email.split('@')[1]
        if company not in stats:
            stats[company] = 0
            print("\tCompany found: ", company)
        stats[company] += 1
    return True, stats


def get_project_stats(pid: int, since: datetime.datetime, until: datetime.datetime):
    args = {'since': since.isoformat(), 'until': until.isoformat(), 'all': 'true'}
    stats = {}

    ok, commits = gitlab_request(f'https://gitlab.com/api/v4/projects/{pid}/repository/commits', args=args)
    if not ok:
        print("ERROR: Unable to get commits of project!")
        return False, None
    for commit in commits:
        email = commit['author_email']
        if '@' not in email:
            print("Malformed email: no @:", email)
            continue
        company = email.split('@')[1]
        # if company not in stats:
            # stats[company] = 0
            # print("\tCompany found: ", company)
        # stats[company] += 1
        if company not in ('maxlinear.com', 'sagemcom.com', 'telecomitalia.it', 'alten.it', 'vodafone.com'):
            continue

        if email not in stats:
            stats[email] = 0
            print("\tEmail found: ", email)
        stats[email] += 1
    return True, stats

start=datetime.datetime(year=2023, month=4, day=1)
end=datetime.datetime(year=2024, month=4, day=1)
token=None

try:
    with open('gitlab_token.txt') as f:
        from datetime import date
        attrs = dict([tuple(x.strip() for x in s.split(':')) for s in f.readlines()])
        token = attrs['Token']
        name = attrs['Name']
        expires = attrs['Expires']
        if not expires or date.fromisoformat(expires) < date.today():
            print("ERROR: token is expired!")
            sys.exit(1)
        print(f"Using token {token} ({name}) which expires on {expires}")
except:
    print("Please make sure you have a valid gitlab_token.txt file. See gitlab_token_example.txt")
    sys.exit(1)

headers['PRIVATE-TOKEN'] = token

proj_nr = 1
projects = get_all_prpl_projects()

for p in sorted(projects, key=lambda x: x['path_with_namespace']):
    pid=int(p['id'])
    path=p['path_with_namespace']
    subgroup=path.split('/')[1]
    if subgroup not in buckets:
        print("000", path, "SKIP")
        continue
    if subgroup not in commit_stats:
        commit_stats[subgroup] = {}
    print("%03d" % (proj_nr), p['path_with_namespace'], datetime.datetime.now().isoformat())

    ok, stats = get_project_stats(pid=pid, since=start, until=end)
    if not ok:
        print(f"Getting events for project {x['path_with_namespace']} failed!")
        continue

    for company, count in stats.items():
        if company not in commit_stats[subgroup]:
            commit_stats[subgroup][company] = count
        else:
            commit_stats[subgroup][company] += count

    print("%03d" % (proj_nr), p['path_with_namespace'], f"{len(stats)} contributers")
    print()

    proj_nr += 1


print()
try:
    with open(csv_filename, 'w', newline='\n') as csv:
        print('group,company,"commit count"', file=csv)
        company_totals = {}
        for subgroup in buckets:
            for company in sorted(commit_stats[subgroup], key=lambda x:commit_stats[subgroup][x], reverse=True):
                print(f"{subgroup},{company},{commit_stats[subgroup][company]}", file=csv)
                if company not in company_totals:
                    company_totals[company] = commit_stats[subgroup][company]
                else:
                    company_totals[company] += commit_stats[subgroup][company]
        for company in sorted(company_totals, key=lambda x:company_totals[x], reverse=True):
            print(f"total,{company},{company_totals[company]}", file=csv)
        print(f"Output written to {csv_filename}")
except:
    print(f"Unable to write output to {csv_filename}:", sys.exception())
