#!/usr/bin/env python3
import requests
import argparse

headers={}
PRPL_GROUP_ID=6326218
RESULTS_PER_REQUEST=100 # 100 is the maximum, and probably the most efficient
description="Change gitlab project settings to conform to the standard prpl settings."

settings={
    'issues_access_level': 'disabled',                # Disables gitlab issues since we use JIRA instead
    'only_allow_merge_if_pipeline_succeeds': 'false', # Needed for projects that don't have pipelines (most of them)
    'only_allow_merge_if_all_discussions_are_resolved': 'true',
#    'merge_method': 'rebase_merge',                   # FF merge hides the history, rebase_merge is preferred since you can make sure that the target branch would build after this merge request builds and merges.
    'lfs_enabled': 'true',                            # Large File Support. I don't know if there is any downside to just enabling it, even for projects that don't need it
    'squash_option': 'never',                          # If developers want to squash commits before pushing them they can still do it, but let's not squash any commits that were pushed to gitlab
    'visibility': 'public',                            # Required by our deal with gitlab, since we're using the open source tier
    'service_desk_enabled': 'false',                   # Not used within prpl (use JIRA/slack instead)
    'public_jobs': 'true',                           # Any jobs/builds made for prpl projects should be public; anything under NDA can be done in a non-prpl repo  (?)
#    'merge_trains_enabled': 'true',
    'merge_requests_access_level': 'private',          # Only prpl members get access to MRs
    'security_and_compliance_access_level': 'private',
    'shared_runners_enabled': 'true',
    'snippets_access_level': 'enabled',                # Can be used for examples, demo scripts, etc
    'request_access_enabled': 'false',                 # Request access by email to info@prplfoundation.org instead
    'requirements_access_level': 'disabled',           # We use JIRA for these
    'repository_access_level': 'enabled',              # Everyone can view our repository
    'monitor_access_level': 'private',
    'model_experiments_access_level': 'disabled',
    'model_registry_access_level': 'disabled',
    'feature_flags_access_level': 'disabled',
    'environments_access_level': 'private',
    'releases_access_level': 'enabled',                # Everyone can use our releases
    'pages_access_level': 'public',                    # Pages can be used to e.g. publish auto-generated Ambiorix documentation
    'packages_enabled': 'true',                        # Enable package registry
    'builds_access_level': 'private',                  # Who can trigger CI/CD pipelines
    'container_registry_access_level': 'enabled',      # Container registry: should it be public ('enabled') or 'private'?
    'infrastructure_access_level': 'disabled',
    'wiki_access_level': 'enabled',
    'analytics_access_level': 'private',
    'remove_source_branch_after_merge': 'true',
    'resolve_outdated_diff_discussions': 'true',
}


def gitlab_request(base_uri: str, args: dict):
    args['per_page'] = str(RESULTS_PER_REQUEST) 
    done = False
    page = 1
    response = []
    while not done:
        args['page'] = str(page)
        urlargs = '&'.join('{}={}'.format(k, args[k]) for k in args)
        req=requests.get(base_uri + '?' + urlargs, headers=headers)
        if not req.ok:
            print("Error: %d %s" %(req.status_code, req.reason))
            print(req.text)
            return False, response
        if not req.headers['x-next-page']:
            done = True
        else:
            page = int(req.headers['x-next-page'])
        response += req.json()

    return True, response


def gitlab_change_project_settings(project_id: int, settings: dict):
    req = requests.put(f'https://gitlab.com/api/v4/projects/{project_id}', headers=headers, data=settings)
    if not req.ok:
        print("PUT request failed: %d %s" %(req.status_code, req.reason))
        print(req.text)

    return req.ok


def get_prpl_projects(filter: str):
    args = {'simple': 'false', 'include_subgroups': 'true'}
    if filter:
        args['search'] = filter

    ok, projects = gitlab_request(f'https://gitlab.com/api/v4/groups/{PRPL_GROUP_ID}/projects', args)
    if not ok:
        print("Unable to get projects of prplfoundation group")
        sys.exit(1)
    return projects


parser = argparse.ArgumentParser(description=description)
parser.add_argument('-p', '--project', type=str, default=None,\
    help='The project to apply these settings to (default: all of them)')
parser.add_argument('-n', '--dry_run', action='store_true',\
    help="Don't actually apply changes, but print what would have been done")
parser.add_argument('-t', '--token', type=str, default=None,\
    help="The gitlab API token to use. If not provided, will read gitlab_token.txt")
args = parser.parse_args()

projects = get_prpl_projects(args.project)
proj_nr = 1
changes = 0

token = None
if args.token:
    token = args.token
else:
    with open('gitlab_token.txt') as f:
        from datetime import date
        attrs = dict([tuple(x.strip() for x in s.split(':')) for s in f.readlines()])
        token = attrs['Token']
        name = attrs['Name']
        expires = attrs['Expires']
        if not expires or date.fromisoformat(expires) < date.today():
            print("ERROR: token is expired!")
            sys.exit(1)
        print(f"Using token {token} ({name}) which expires on {expires}")

headers['PRIVATE-TOKEN'] = token

for p in sorted(projects, key=lambda x: x['path_with_namespace']):
    pid=int(p['id'])
    divergent=False
    print("%03d" % (proj_nr), p['path_with_namespace'])
    for key in settings:
        if key in p and str(p[key]).lower() != settings[key].lower():
            print(f"\t{key}: {p[key]} => {settings[key]}")
            divergent=True

    if not divergent:
        print("%03d" % (proj_nr), p['path_with_namespace'], "no divergent settings detected")
    elif args.dry_run:
        print("%03d" % (proj_nr), p['path_with_namespace'], "would have been updated")
        changes += 1
    else:
        ok = gitlab_change_project_settings(pid, settings)
        print("%03d" % (proj_nr), p['path_with_namespace'], "updated successfully" if ok else "FAIL")
        changes += 1
    print()

    proj_nr += 1

print()
print(f"{changes}/{proj_nr-1} projects %s updated." %("would be" if args.dry_run else ""))
