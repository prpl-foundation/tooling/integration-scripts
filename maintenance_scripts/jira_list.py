import requests
import argparse
import json

headers={}

description='''
Get a list of JIRA IDs based on project and fix version.
'''

parser = argparse.ArgumentParser(description=description)
parser.add_argument('-p', '--project', type=str, required=True,\
    help='The project to use (eg: PPM, PCF, ...)')
parser.add_argument('-u', '--user', type=str, default=None,\
    help="The JIRA user to use. If not provided, will read jira_token.txt. Needs -t to be provided as well")
parser.add_argument('-t', '--token', type=str, default=None,\
    help="The JIRA API token to use. If not provided, will read jira_token.txt. If provided, make sure to provide -u as well")
parser.add_argument('-v', '--fixVersion', help="The 'fix version' to filter for", type=str, required=True)
args = parser.parse_args()

if bool(args.user) != bool(args.token):
    raise ValueError("If -u or -t are provided, the other should be provided as well")
if args.user:
    user=args.user
    token=args.token
else:
    with open('jira_token.txt') as f:
        from datetime import date
        attrs = dict([tuple(x.strip() for x in s.split(':')) for s in f.readlines()])
        token = attrs['Token']
        name = attrs['Name']
        user = attrs['User']
        expires = attrs['Expires']
        if not expires or date.fromisoformat(expires) < date.today():
            print("ERROR: token is expired!")
            sys.exit(1)
        print(f"Using token {token} ({name}) which expires on {expires}")


base_uri='https://prplfoundationcloud.atlassian.net/rest/api/3/search/jql'
headers['Content-Type'] = 'application/json'
params={}
params['jql'] = f"fixVersion = \"{args.fixVersion}\" ORDER by key ASC"
params['fields']='key,issuetype,summary'
params['fieldsByKeys']='true'
cont=True

nextPageToken=None
issues=[]
while cont:
    if nextPageToken:
        params['nextPageToken'] = nextPageToken
    urlargs = '&'.join('{}={}'.format(k, params[k]) for k in params)
    req=requests.get(base_uri + '?' + urlargs, headers=headers, auth=(user,token))
    if not req.ok:
        break
    req_json=req.json()
    issues += req_json['issues']
    if not 'nextPageToken' in req_json:
        cont=False
    else:
        nextPageToken = req_json['nextPageToken']

print(f"{len(issues)} issues found.")
print()
for issue in issues:
    print(issue['key'], issue['fields']['issuetype']['name'], issue['fields']['summary'])


