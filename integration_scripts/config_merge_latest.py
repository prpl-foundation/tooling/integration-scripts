#!/usr/bin/env python3
""" Config Merge Latest
This script will prepare the MR for config repositories. These repositories stack CI updates in a
"latest" branch, which is offered to a source branch every night. To keep the MR stable, a separate
"ci_latest" branch is created which remains stable until the next night.
An alternate mode for this script will simply prepare the "latest" branch for merging, by making
sure all changes manually made to the source branch are merged into the integration branch.
A dummy MR is created to trigger the regendefconfig process.
"""
import argparse
import re
import time
import os
from concurrent.futures import ThreadPoolExecutor
from typing import List, Optional, Tuple

from sahlab import list_projects
from sahlab.repository import (ConfigRepo, SAHRepo, WrtConfigRepo,
                               YoctoDistroRepo, PrplConfigRepo)
from sahlab.review import Review
from sahlab.user import list_user_ids
from sahlab.haystack import Haystack
from sahlab.gitlab import use_instance

from integration_scripts import integration_exit, logger, safe_integration
from integration_scripts.merge_strategy import (
    determine_merge_config_from_repo, merge_when_pipeline_succeeds)
from integration_scripts.utilities import now, determine_api_key

PARSER = argparse.ArgumentParser(description='SAH Config Merge Tool',
                                 prog='sah-config-merge-latest')
PARSER.add_argument("--pre",
                    help="Perform pre-config-merge-latest to prepare the integration branches",
                    action='store_true',
                    default=False)
PARSER.add_argument("--namespace-filter",
                    type=str,
                    dest='namespace_filter',
                    default=".*",
                    help="Regex string to match the eligible config namespaces to")
PARSER.add_argument("--name-filter",
                    type=str,
                    dest='name_filter',
                    default=".*",
                    help="Regex string to match the eligible config names to")
PARSER.add_argument("--branch",
                    type=str,
                    help="Singular branch to create MR for, useful for manual runs")
PARSER.add_argument("--haystack-branch",
                    help="The branch on the haystack to use",
                    dest='haystack_branch',
                    type=str,
                    default=os.environ.get("HAYSTACK_BRANCH", "master"))
PARSER.add_argument("--haystack-repo-id",
                    help="The id of the haystack to use",
                    dest='haystack_repo_id',
                    type=int,
                    default=6729)
PARSER.add_argument("--git",
                    help="The gitlab instance where the component lives",
                    dest='git',
                    type=str,
                    default="gitlabinternal.softathome.com")
PARSER.add_argument("--no-dry-run",
                    help="Actually create MR",
                    dest='dry_run',
                    action='store_false',
                    default=True)
PARSER.add_argument("--verbose", "-v",
                    help="Print more information",
                    action='store_true',
                    default=False)
PARSER.add_argument("--thread-factor",
                    help="Number of threads to spawn (this is applied twice, once per project, "
                         "once per branch",
                    dest='thread_factor',
                    default=16)
ARGS = PARSER.parse_args()


use_instance(instance=f"https://{ARGS.git}",
             api_token=determine_api_key(f"https://{ARGS.git}"))
LOG = logger.get_logger(ARGS.verbose)
HAY = Haystack(branch=ARGS.haystack_branch, repo=ARGS.haystack_repo_id)


def _get_owners(project: SAHRepo, target_branch: str, stable: str) -> Tuple[List[str], List[str]]:
    """ List the owners of the target_branch of the given repo, depending on the changes
    between stable and target_branch.

    Returns:
        Tuple[List[str], List[str]:
            0: List of owners
            1: List of updated owned files
    """
    diffs = project.project.repository_compare(target_branch, stable)['diffs']
    files = [diff['new_path'] for diff in diffs if diff['new_path'].endswith(("project.list",
                                                                              "feeds.conf"))]
    return project.get_owners(ref=target_branch, updated_files=files), files


def _construct_description(project: SAHRepo, target_branch: str, stable: str) -> str:
    """ Constructs description based on all commits in between stable and target_branch.
    Issues are prefixed with a `* ` to put them in a list.
    """
    commits = project.project.repository_compare(target_branch, stable)['commits']
    return re.sub(r'^Issue: ',
                  r'* Issue: ',
                  "\n\n".join([commit['message'] for commit in commits]),
                  flags=re.MULTILINE)


def _list_projects(target, class_hint):
    return list_projects(f'{target} | select(.name | match("{ARGS.name_filter}";"i"))'
                         f'| select(.namespace | match("{ARGS.namespace_filter}";"i"))'
                         f'| select(.ci != false)', class_hint, haystack=HAY)


@safe_integration
def pre_merge_latest(project: SAHRepo, branch: str, target_branch: str) -> Optional[Review]:
    """ Creates a dummy MR for rebasing purposes. This MR will also trigger the regendefconfig
    process on Jenkins. Once rebased, it can be removed again.
    """
    title = f"[CI] {now()}: Rebase {branch} on {target_branch}"
    LOG.info("Pre-merge Title: %s", title)
    if ARGS.dry_run:
        return None
    review = project.create_mr(branch, target_branch,
                               title=title, description="Rebasing before creating real MR")
    review.mr_obj.rebase()
    review.close()
    review.delete()

    return review


def _is_up_to_date(project: SAHRepo, branch: str, stable: str, target_branch: str) -> bool:
    """ Checks if the branches (branch/stable) are up to date with the target branch. """
    LOG.info("Checking up-to-dateness of branch: %s, stable: %s, target_branch: %s",
             branch, stable, target_branch)
    if not project.has_branch(target_branch):
        LOG.error("Target branch %s doesn't exist! Abuse of latest naming scheme detected?",
                  target_branch)
        return True
    if not ARGS.pre and project.has_branch(stable) \
            and project.get_sha(stable) == project.get_sha(branch):
        LOG.info("%s is up to date with %s, skipping...", stable, branch)
        return True
    if (ARGS.pre or not project.has_branch(stable)) \
            and (project.is_merged(branch)
                 or not project.project.repository_compare(target_branch, branch)['commit']):
        LOG.info("%s is up to date with %s, skipping...", branch, target_branch)
        return True
    return False


def _reset_stable_branch(project: SAHRepo, stable: str, branch: str, target_branch: str):
    """ Resets the stable branch to branch. Any existing MR to target_branch are closed. """
    if project.has_branch(stable):
        review = project.get_review(stable, target_branch)
        if review:
            review.close()
        project.delete_branch(stable)
    project.create_branch(stable, branch)


@safe_integration
def merge_latest_branch(project: SAHRepo, branch: str) -> Optional[Review]:
    """ Prepares MR for a single latest branch (ci_)(source_)latest(_board) to source (master).
    If this is a 'pre' run, the MR is only used for triggering a regendefconfig (and conveniently
    for a rebase). """
    stable = "ci_" + branch
    LOG.info("Handling Branch: %s", branch)
    LOG.info("Stable Branch: %s", stable)
    match = re.match(r'(s_)?(?P<source>.*_)?latest(?P<board>_.*)?', branch)
    target_branch = match.group('source')[:-1] if match and match.group('source') else "master"
    board = match.group('board')[1:] if match and match.group('board') else None
    # prpl case
    if re.match(r"^latest-[0-9]+\.[0-9]+$", branch):
        target_branch = "mainline" + branch[6:]

    LOG.info("Target Branch: %s", target_branch)
    if _is_up_to_date(project, branch, stable, target_branch):
        return None
    LOG.info("Board: %s", board)

    if ARGS.pre:
        if isinstance(project, (ConfigRepo, WrtConfigRepo, PrplConfigRepo, YoctoDistroRepo)):
            return pre_merge_latest(project, branch, target_branch)
        return None

    if not ARGS.dry_run:
        # Reset stable integration branch and MR
        _reset_stable_branch(project, stable, branch, target_branch)
    elif not project.has_branch(stable):
        stable = branch

    description = _construct_description(project, target_branch, stable)
    owners, files = _get_owners(project, target_branch, stable)
    title = f"[CI] {now()}: Integrate latest changes ({stable} -> {target_branch})"

    if ARGS.dry_run:
        LOG.info("DRYRUN: title: %s", title)
        LOG.info("DRYRUN: owners: %s", owners)
        LOG.info("DRYRUN: description:\n ****** \n%s\n ****** \n", description)
        return None

    review = project.create_mr(stable, target_branch,
                               title=title, description=description,
                               labels=["config_ci"], assignee_ids=list_user_ids(owners))
    LOG.info("Merge request available at: %s", review.get_attribute('web_url'))
    # Determine auto_merging
    merge_strategies = determine_merge_config_from_repo(project, target_branch, LOG, files)
    if merge_strategies and all(mc.auto_merge for mc in merge_strategies):
        merge_when_pipeline_succeeds(review, LOG)
    return review


def merge_latest(project: SAHRepo) -> List[Review]:
    """ Determines all relevant branches and spawns a thread to handle it. """
    LOG.info("Processing: %s/%s", project.definition['namespace'], project.definition['name'])
    branches = [ARGS.branch] if ARGS.branch else project.list_branches(r'(?!dev_)(?!ci_).*latest')

    thread_name_prefix = f"merge_latest_{project.definition['namespace'].replace('/', '_')}_" \
                         f"{project.definition['name']}"
    with ThreadPoolExecutor(max_workers=int(ARGS.thread_factor),
                            thread_name_prefix=thread_name_prefix) as executor:
        futures = [executor.submit(merge_latest_branch, project, branch) for branch in branches]

    return [r for r in map(lambda f: f.result(), futures) if r]


def main():
    """ Main entrypoint of sah-config-merge-latest
    This method will process the desired integration branches of config repositories.
    """
    start_time = time.time()

    projects = _list_projects('."openwrt-config"[]', WrtConfigRepo) \
        + _list_projects(".config[]", ConfigRepo) \
        + _list_projects('."yocto-distro"[]', YoctoDistroRepo) \
        + _list_projects('."prpl-config"[]', PrplConfigRepo)

    with ThreadPoolExecutor(max_workers=int(ARGS.thread_factor),
                            thread_name_prefix='merge_latest') as executor:
        futures = [executor.submit(merge_latest, project) for project in projects]

    res = []
    for future in futures:
        res.extend(future.result())

    elapsed_time = time.time() - start_time
    LOG.info(f"Finished processing! Prepared {len(list(filter(None, res)))} MR!"
             f" Elapsed time {time.strftime('%Mm%Ss', time.gmtime(elapsed_time))}.")


def run():
    """ Runs the main entrypoint and exists appropriately. """
    main()
    integration_exit()


if __name__ == '__main__':
    run()
