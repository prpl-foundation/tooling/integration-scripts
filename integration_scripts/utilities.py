""" Utilities
Simple extra reusable functions that don't have anywhere else to live.
"""
import os
import re
from datetime import datetime
from typing import Any, Dict, Optional, Union

import yaml
import jenkins
from jinja2 import Environment, FileSystemLoader
from sahlab.gitlab import Gitlab
from sahlab.group import Group
from sahlab.naming import Name, NameFactory, SAHTag
from sahlab.repository import ConfigRepo, SAHRepo, WrtConfigRepo


def now(style: str = "%c") -> str:
    """ Get string representation of the now. """
    return datetime.utcnow().strftime(style)


def get_name_for_component(name_factory: NameFactory, component: str,
                           git: str = "gitlabinternal.softathome.com") -> Name:
    """ Wrapper function around the SAHLab NameFactory get_name_for_component function.
    This function will first attempt a strict name retrieval. If this fails, it will fallback to
    a fuzzy search.
    """
    component_names = name_factory.get_name_for_component(component, git=git)
    if not component_names:
        component_names = name_factory.get_name_for_component(component, fuzzy_search=True, git=git)
    assert len(component_names) == 1, \
        f"Given component name {component} not specific enough, matches none or multiple results: "\
        f"'{component_names}'"
    return component_names[0]


def parse_sahbot(sahbot_conf: Optional[dict], path: str, default: Any, log) -> Any:
    """ Parses the given sahbot dictionary to get the value on the given path. If it doesn't exist,
    it returns the given default value. """
    if not sahbot_conf:
        return default
    value = sahbot_conf
    for elem in re.split(r'(?<!\\)\.', path):
        elem = elem.replace(r'\.', '.')
        try:
            value = value[elem]
        except (KeyError, TypeError, IndexError):
            log.info("Failed to find %s (%s) in %s -- defaulting to %s", elem, path, value, default)
            return default
    return value


def cleanup_branch(repo: SAHRepo, branch: str):
    """ Removes branch if it exists. """
    if repo.has_branch(branch):
        repo.delete_branch(branch)


def determine_api_key(url):
    """ Determine which gitlab api key to use based on a gitlab url
    e.g. https://gitlab.com/prpl-foundation/...
    """
    instance = url.replace("https://", "").split("/")[0]
    if instance == "gitlabinternal.softathome.com":
        return os.environ.get("GITLAB_API_KEY")
    if instance == "gitlab.com":
        return os.environ.get("GITLAB_API_KEY_EXTERNAL")
    return os.environ.get(f"GITLAB_API_KEY_{instance.replace('.', '_')}")


def http_to_oauth(url: str) -> str:
    """ Injects the oauth2 authentication string in an https url. """
    return f'https://oauth2:{determine_api_key(url)}@{url[8:]}'






def gather_sahbot_yml(repo: SAHRepo, ref: str, mr_description: str = "") -> Optional[dict]:
    """ Returns the dict version of the .sahbot.yml of the given repo on the given ref.
    Optionaly a MR description can be passed to the function, this description can contain a
    .sahbot.yml section, which overrides the defaults found on the branch.

    e.g.

    .sahbot.yml
    ```
    build:
      linked:
      - feed: feed_amx
      - feed: feed_net_core
    ```
    """
    if not repo.has_file(".sahbot.yml", ref):
        return None

    sahbot = yaml.safe_load(repo.get_raw_file(".sahbot.yml", ref))

    match = re.search('.?sahbot.yml\n```(.*)```', mr_description, flags=re.DOTALL | re.MULTILINE)
    if match:
        result = yaml.safe_load(match.group(1))
        merge_dicts(sahbot, result)

    return sahbot












def use_jenkins_instance(username, api_token,
                         instance: str = "https://sah2jenkinshgw.be.softathome.com"):
    """ Authenticates against the specified Jenkins instance. """
    # Ensure the server URL uses HTTPS
    if instance.startswith('http://'):
        instance = 'https://' + instance[len('http://'):]
    elif not instance.startswith('https://'):
        instance = 'https://' + instance

    return jenkins.Jenkins(instance, username=username, password=api_token)
