""" SAH Integration Scripts - Integrate repositories and other automation tools """
import sys
import traceback

from sahlab.repository import Dependency

DELAYED_FAILURE = False


def safe_integration(func):
    """ Decorator function to catch exceptions during integration so other integrations aren't
    prematurely stopped. The error is printed to the terminal and the program will exit with
    a failure if `integration_scripts.integration_exit` is used to exit. """
    def safe_func(*args, **kwargs):
        # pylint: disable=W0603 # Using global stmnt for keeping track of delayed terminal failure
        global DELAYED_FAILURE
        try:
            return func(*args, **kwargs)
        # pylint: disable=W0703 # Broad except for making sure we're not terminating too soon
        #                         Exception is not ignored
        except Exception:
            print(f"Failed to complete {func.__name__} with arguments: {list(map(str, args))}")
            traceback.print_exc()
            DELAYED_FAILURE = True
            return None
    return safe_func


def integration_exit(code: int = 0):
    """ Exits with the given code. If the code is 0, yet DELAYED_FAILURE is set, the program will
    exit with code 1. """
    if DELAYED_FAILURE and not code:
        code = 1
    sys.exit(code)
