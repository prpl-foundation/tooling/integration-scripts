#!/usr/bin/env python3
""" Component Integrate
This script handles integration of a new version of a component/repo to anything that depends on
the given component/repo. The script will look for anything using the component
(only (wrt)configs/metas by default, but this can be extended to more targets if desired).
Next, new commits will be made to update the version to the new/given version.
The version is only updated, if the base of the original matches the new.
Finally, a MR will be created depending on the merge strategy for the given component.
"""
import argparse
import os
import re
import shutil
import subprocess
import time
from collections import namedtuple
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from smtplib import SMTPRecipientsRefused
from typing import Dict, List, Optional, Tuple

import requests
from git import Repo  # type: ignore
from git.exc import BadName, GitCommandError  # type: ignore
from gitlab.exceptions import GitlabCreateError, GitlabGetError
import sahlab
from sahlab.gitlab import use_instance
from sahlab.issue import Issue
from sahlab.naming import Name, NameFactory, SAHTag
from sahlab.repository import (CommitBuilder, ConfigRepo, Dependency, MetaRepo,
                               OpenwrtFeedRepo, PlainRepo, SAHRepo,
                               WrtConfigRepo, YoctoDistroRepo, YoctoLayerRepo, PrplConfigRepo,
                               repo_create_by_id, repo_create_by_name)
from sahlab.review import Review
from sahlab.user import list_user_ids
from sahlab.haystack import Haystack

from integration_scripts import integration_exit, logger, safe_integration
from integration_scripts.merge_strategy import (
    MergeStrategy,
    merge_when_pipeline_succeeds,
    determine_merge_config_from_dep)
from integration_scripts.utilities import (cleanup_branch,
                                           get_name_for_component,
                                           http_to_oauth, determine_api_key)

PARSER = argparse.ArgumentParser(description='SAH Component Integrator',
                                 prog='sah-component-integrate')
PARSER.add_argument("component",
                    type=str,
                    help="The component's name, can be fuzzily defined")
PARSER.add_argument("tag",
                    type=str,
                    help="The new tag to integrate to, should be a valid SAHTag")
PARSER.add_argument("--from-path",
                    help="The given component is actually a git path to a repository",
                    dest='from_path',
                    action='store_true',
                    default=False)
PARSER.add_argument("--git",
                    help="The gitlab instance where the component lives",
                    dest='git',
                    type=str,
                    default="gitlabinternal.softathome.com")
PARSER.add_argument("--haystack-repo-id",
                    help="The id of the haystack to use",
                    dest='haystack_repo_id',
                    type=int,
                    default=6729)
PARSER.add_argument("--haystack-branch",
                    help="The branch on the haystack to use",
                    dest='haystack_branch',
                    type=str,
                    default=os.environ.get("HAYSTACK_BRANCH", "master"))
PARSER.add_argument("--mappings-repo-id",
                    help="The id of the haystack to use",
                    dest='mappings_repo_id',
                    type=int,
                    default=8514)
PARSER.add_argument("--commit-author",
                    help="Name of the author of commits made",
                    dest='commit_author',
                    type=str,
                    default="sahbot")
PARSER.add_argument("--commit-author-email",
                    help="Email of the commit author of commits made",
                    dest='commit_author_email',
                    type=str,
                    default="sahbot@softathome.com")
PARSER.add_argument("--baf-dir",
                    type=str,
                    help="The root directory of the baf.yml where generation happened",
                    dest='baf_dir',
                    default="")
PARSER.add_argument("--dry-run",
                    help="Do not actually update any repository",
                    dest='dry_run',
                    action='store_true',
                    default=False)
PARSER.add_argument("--assignees",
                    help="Set assignee (username) of generated Merge Request if not owned",
                    nargs="+",
                    default=[])
PARSER.add_argument("-v", "--verbose",
                    help="Print more information",
                    action='store_true',
                    default=False)
PARSER.add_argument("--thread-factor",
                    help="Number of threads to spawn",
                    dest='thread_factor',
                    default=16)
ARGS = PARSER.parse_args()

WSDIR = Path('/tmp/_sahlab_integration_scripts/component_integrate')

NAME_FACTORY = NameFactory(provide_default_mapping=False)


LOG = logger.get_logger(ARGS.verbose)

use_instance(instance=f"https://{ARGS.git}",
             api_token=determine_api_key(f"https://{ARGS.git}"))
if ARGS.git != "gitlabinternal.softathome.com":
    NAME_FACTORY.provide_gitlab_mappings(branch="main", repo=ARGS.mappings_repo_id,
                                         file="Gitlab.csv", git=ARGS.git)

HAY = Haystack(branch=ARGS.haystack_branch, repo=ARGS.haystack_repo_id)

DependencyGroup = namedtuple('DependencyGroup',
                             'dependant_name strategy dependencies')


class ConflictError(Exception):
    """ Custom Exception to indicate updating a repository failed due to a merge conflict.
    The receiver of the exception can choose to retry the update or to give up on the integration.
    """


def group_dependencies(dependencies: List[Dependency]) -> List[DependencyGroup]:
    """ Group dependencies by dependant as a primary key and target/integration_branch as a
    secondary key. All dependencies that go to the same target, should be grouped. This is primarily
    useful for integrating to a config, as a ConfigRepo tends to house multiple configs
    (project.list) files, that we want to update simultaneously.
    """
    # dict(groups=dict(integration_branch=list(Dependency)))
    encountered: Dict[str, Dict[str, List[Dependency]]] = {}
    strategies = {}
    for dep in dependencies:
        try:
            strategy = determine_merge_config_from_dep(dep, LOG)
        except (AttributeError, ValueError) as err:
            LOG.error("Failed to determine merge strategy for %s, invalid .sahbot.yml?", str(dep))
            LOG.error(str(err))
            continue
        name = dep.dependant.get_attribute("path_with_namespace")
        integration_branch = strategy.branch
        strategies[name + "__" + integration_branch] = strategy
        if name in encountered:
            group = encountered[name]
            if integration_branch in group:
                group[integration_branch].append(dep)
            else:
                group[integration_branch] = [dep]
        else:
            encountered[name] = {integration_branch: [dep]}
    dependency_groups = []
    for dependant, dependencies_by_ref in encountered.items():
        for ref in dependencies_by_ref:
            dependency_groups.append(DependencyGroup(dependant,
                                                     strategies[dependant + "__" + ref],
                                                     dependencies_by_ref[ref]))
    return dependency_groups


def setup_workdir(dep: Dependency, integration_branch: str) -> Tuple[Path, Repo]:
    """ Sets up local files required for the integration. These files are stored in the workdir.
    This workdir is assured to be unique for this integration as seperate workdirs are created for
    other processes, dependants and integration branch combinations.

    The workdir is assured to be clean and will contain a cloned repo of the dependant, with the
    integration branch checked out. It is assumed this branch already exists.
    """
    parent = dep.dependant
    if not isinstance(dep.dependant, MetaRepo):
        parent_sah_name = dep.dependant.definition['name']
    else:
        parent_sah_name = get_name_for_component(NAME_FACTORY, parent.definition['name']).sah_name
    workdir = WSDIR.joinpath(Path(str(os.getpid()) + "_"
                                  + parent.definition['namespace'].replace('/', '_') + "_"
                                  + parent_sah_name + "_"
                                  + integration_branch))
    shutil.rmtree(str(workdir), ignore_errors=True)
    workdir.joinpath("scripts/kconfig").mkdir(parents=True)

    parent_repo = Repo.clone_from(http_to_oauth(parent.get_attribute('http_url_to_repo')),
                                  str(workdir.joinpath(Path(parent_sah_name))),
                                  branch=integration_branch)
    return workdir, parent_repo




def _reset(repo: Repo, ref: str):
    """ Fetches all remotes and hard resets to the given branch. """
    for remote in repo.remotes:
        remote.fetch()
    repo.git.checkout(ref)
    repo.git.reset('--hard', f"origin/{ref}")




def _merge_staging_onto_integration_branch(repo: Repo, dependant_ref: str,
                                           staging_ref: str, target_ref: str) -> bool:
    """ Some git-fu to attempt to cleanly integrate a commit onto the integration branch,
    without running into race conditions.
    It works as follows:
    1. Any diverged changes from the final source (target?) branch should be included in the
       integration branch. This is done with a recursive merge, with a preference for the upstream
       changes, from the source branch into the integration branch. If the source branch hasn't
       diverged, then this operation does nothing. The integration branch should be pushed if it
       was updated. It is possible this fails, if that is the case, then the caller should simply
       attempt again from a reset state (the cause of the failure will always be changes pushed to
       the integration branch that we missed, a retry will pick these up, eventually this should
       stabilize).
    2. Rebase the new commit (staging ref) on top of the integration branch. This is safe as the
       staging commit should be unique and only handled by this process
       (it is unlikely multiple integrations for the same component happen concurrently).
       The HEAD of the repo will now be detached on the staging commit, placed in-line with the
       integration branch.
       If this fails, then there is likely a conflict between the newly created integration commit
       and the, recently updated, integration branch. This conflict cannot be resolved cleanly and
       automatically from this state. The staging commit should be removed and integration should
       start again from the regendefconfig stage. The new integration will take the updated
       integration branch into account and will not result in another conflict (assuming not yet
       another conflicting integration happens after pushing the new commit).
    3. The integration branch can now be fast-forwarded to the position of our HEAD.
    4. The commit has now been integrated cleanly and can be pushed as part of the integration
       branch. If the push fails, then another integration beat us to it. If that is the case, we
       should simply retry this again.
    """
    # reset
    _reset(repo, target_ref)

    # Merge source branch to integration branch to pick up upstream changes
    out = repo.git.merge('-s', 'recursive', '-X', 'theirs',
                         f"origin/{dependant_ref}", target_ref)
    if out != "Already up to date.":
        repo.git.push('origin', target_ref)

    # rebase staging branch onto integration branch
    repo.git.rebase(target_ref, f"origin/{staging_ref}")

    # ff rebase integration branch on staging branch
    repo.git.rebase('--ff', "HEAD", target_ref)

    # push integration branch
    repo.git.push('origin', target_ref)

    # cleanup staging branch
    repo.git.push('origin', f":{staging_ref}")
    return True


def _dependency_already_prepared(dep: Dependency, new_version: SAHTag,
                                 integration_branch: str) -> bool:
    """ Checks if the dependency has already been prepared on the integration branch. """
    if dep.dependant.has_branch(integration_branch):
        dependencies = dep.dependant.get_dependencies(integration_branch)
        prepared_versions = []
        for ex_dep in dependencies:
            if ex_dep.name == dep.name:
                try:
                    prepared_versions.append(SAHTag(ex_dep.ref))
                except ValueError:
                    pass
        prepared_versions = [v for v in prepared_versions
                             if v.get_branch() == new_version.get_branch()]
        return all(prepared_version >= new_version for prepared_version in prepared_versions)
    return False


def _get_baf_output(baf_dir: str, target_dir: str) -> List[Tuple[str, str]]:
    """ Add the baf files to list of files to be updated. """
    files: List[Tuple[str, str]] = []
    subdir_path = Path(baf_dir)
    for subfile in subdir_path.rglob("*"):
        files.append((str(Path(target_dir) / subfile.relative_to(subdir_path)),
                      subfile.read_text()))
    return files


def _update_files(repo: SAHRepo, commit_builder: CommitBuilder,
                  files: List[Tuple[str, str]]) -> List[str]:
    """ Update commit with new contents of given files, create the file if it doesn't exist """
    updated_files = []
    for file_path, contents in files:
        if ARGS.dry_run:
            LOG.info(f"DRYRUN: {file_path}\n ****** \n{contents}\n ****** \n")
        try:
            upstream_contents = repo.get_raw_file(file_path, commit_builder.start_sha)
        except GitlabGetError:
            LOG.info(f"File {file_path} not found in {str(repo)} on "
                     f"{commit_builder.start_sha}, creating...")
            commit_builder.create(file_path, contents)
            updated_files.append(file_path)
            continue
        if contents != upstream_contents:
            commit_builder.update(file_path, contents)
            updated_files.append(file_path)
    return updated_files


# pylint: disable=too-many-locals
def update_remote(deps: List[Tuple[Dependency, SAHTag]],  # pylint: disable=too-many-branches
                  opensource_version: str,
                  target_ref: str, issues: List[Issue], parent_repo: Repo,
                  retries: int) -> Optional[List[str]]:
    """ Update the list of dependencies to the new version. A single commit will be created,
    updating the dependency files as well as any updated files in the local parent repository.
    These local files can be regenerated defconfig fragments. The commit message will indicate
    which component is updated, to which version and which issues are handled by this integration.
    The final commit is pushed to a new "staging" branch. This branch/commit is then attempted to
    be rebased/merged cleanly with the integration branch without losing information due to race
    conditions. If the merge fails due to changes pushed to the integration branch from another
    source, the merge can be retried. If the merge fails due to a conflict, then we raise a
    ConflictError and the integration should start again (if allowed by the number of retries).

    Finally, a list of all updated files is returned. This list can be used to determine ownership
    of the files when creating a merge request.
    """
    updated_files: List[str] = []
    if not deps:
        LOG.warning("No dependencies to update!")
        return updated_files

    msg = f"Integrating {deps[0][0].name} to {str(deps[0][1])}\n\n" + "\n".join(map(str, issues))

    LOG.info(f"Integration branch: {target_ref}")
    staging_ref = f"dev_{deps[0][0].name.replace(':', '_').replace('/', '_')}"\
                  f"_{str(deps[0][1])}_staging_{target_ref}"
    LOG.info(f"Staging branch: {staging_ref}")
    if ARGS.dry_run:
        LOG.info(f"DRYRUN: Commit msg:\n ****** \n{msg}\n ****** \n")

    parent_sha = deps[0][0].dependant.get_sha(target_ref)
    commit_builder = CommitBuilder().with_target_branch(staging_ref) \
                                    .from_commit(parent_sha) \
                                    .with_author(ARGS.commit_author, ARGS.commit_author_email) \
                                    .with_commit_message(msg)
    commit_builder.signoff = True
    for dep in deps:
        if isinstance(dep[0].dependant, YoctoLayerRepo) and ARGS.baf_dir \
           and os.path.isdir(f"{ARGS.baf_dir}/yocto/kirkstone") \
           and any(os.scandir(f"{ARGS.baf_dir}/yocto/kirkstone")):
            # Pick baf output and look for the correct yocto files to use
            match = re.search(r'# YOCTO_VERSION = "(.*)?"',
                              dep[0].dependant.get_raw_file(dep[0].found_in,
                                                            ref=target_ref))
            if match:
                files = _get_baf_output(f"{ARGS.baf_dir}/yocto/{match.group(1)}",
                                        dep[0].found_in.rsplit("/", 1)[0])
            else:
                LOG.warning(f"Cannot find YOCTO_VERSION on {target_ref}, skipping ...")
                files = []
        elif isinstance(dep[0].dependant, OpenwrtFeedRepo) and ARGS.baf_dir \
                and os.path.isdir(f"{ARGS.baf_dir}/openwrt") \
                and any(os.scandir(f"{ARGS.baf_dir}/openwrt")):
            files = _get_baf_output(f"{ARGS.baf_dir}/openwrt/",
                                    dep[0].found_in.rsplit("/", 1)[0])
        else:
            # Update dep directly
            files = [(dep[0].found_in, dep[0].update(str(dep[1]), opensource_version,
                                                     integration_branch=parent_sha))]

        updated_files.extend(_update_files(dep[0].dependant, commit_builder, files))

    # Commit
    if not updated_files or ARGS.dry_run:
        LOG.warning(f"No files to update for {str(deps[0][0].dependant)}, skipping...")
        return updated_files
    # Cleanup old staging ref in case of previous failure
    cleanup_branch(deps[0][0].dependant, staging_ref)
    deps[0][0].dependant.create_commit(commit_builder)

    merged = False
    while not merged and retries > 0:
        try:
            merged = _merge_staging_onto_integration_branch(parent_repo, deps[0][0].dependant_ref,
                                                            staging_ref, target_ref)
        except (BadName, GitCommandError) as error:
            LOG.warning(f"Failed to merge {staging_ref} onto {target_ref}")
            LOG.warning(f"Got Exception: {str(error)}")
            time.sleep(10 / (retries + 1))  # Backoff wait timer
            if parent_repo.currently_rebasing_on() and retries > 0:
                LOG.info("Detected conflict, attemting to integrate again...")
                parent_repo.git.rebase('--abort')
                parent_repo.git.push('origin', f":{staging_ref}")
                raise ConflictError("Cannot update remote, conflict detected!") from error
            retries = retries - 1
    if not merged:
        LOG.error("Unable to complete integration, persistent conflict?")
        return None
    return updated_files


def _determine_owners(repo: SAHRepo, source: str,
                      target: str, updated_files: List[str]) -> List[str]:
    """ Determines the list of owners/assignees for the given repository, between the given refs and
    with the given updated files. """
    owners = repo.get_owners(ref=target, updated_files=updated_files) or ARGS.assignees.copy()
    review = repo.get_review(source, target)
    if review:
        # Combine assignees from pre-existing MR
        owners.extend([ass['username'] for ass in review.get_attribute("assignees")])
    return owners


def setup_merge_request(dep: Dependency, version: SAHTag, target_ref: str,
                        issues: List[Issue], updated_files: List[str],
                        strategy: MergeStrategy, repo: Repo) -> Optional[Review]:
    """ Setups up a merge request for the newly integrated changes. This merge request will
    detail the updated issues handled by this integration. The assignees is determined by the
    repository, in combination with the updated files from this integration.

    The SAHLab Review object is returned.
    """
    if isinstance(dep.dependant, (ConfigRepo, WrtConfigRepo, YoctoDistroRepo, PrplConfigRepo)):
        LOG.info(f"Config MR from ci_latest is handled by another job - Done for now! ({dep.name})")

        return None

    # Check for pre-existing MR
    merge_request = dep.dependant.get_review(target_ref, dep.dependant_ref)
    title = ""
    description = ""
    if merge_request:
        title = merge_request.get_attribute("title")
        description = merge_request.get_attribute("description") + "\n"
        if f"**{dep.name}=" in description:
            description = re.sub(rf'\n\*\*{dep.name}=.*\*\*\n(\*.*\n)+', '',
                                 description, flags=re.MULTILINE)

    # Determine title/description
    title = title or str(issues[0])
    issue_table = "\n* " + "\n* ".join(map(str, issues))
    description = f"{description}\n**{dep.name}={str(version)}**{issue_table}\n"

    # Create MR
    labels = []
    if isinstance(dep.dependant, MetaRepo):
        labels.append('meta')
    if strategy.auto_label:
        labels.append('tag auto')
    if strategy.auto_merge:
        labels.append('auto_merge')
    owners = _determine_owners(dep.dependant, target_ref, dep.dependant_ref, updated_files)

    LOG.info(f"Title: {title}")
    LOG.info(f"Assignees: {owners}")
    if ARGS.dry_run:
        LOG.info(f"DRYRUN: description:\n ****** \n{description}\n ****** \n")
        LOG.info(f"DRYRUN: Labels: {labels}")
        return None
    review = dep.dependant.create_mr(target_ref,
                                     dep.dependant_ref,
                                     title,
                                     description,
                                     labels=labels,
                                     assignee_ids=list_user_ids(owners))
    LOG.info(f"Merge request available at: {review.get_attribute('web_url')}")

    # Auto Merging
    if strategy.auto_merge:
        m_commit = "\n".join(sorted(set(re.findall(r'^\* (Issue.*)',
                                                   description, re.MULTILINE))))
        m_commit = m_commit + f"\nSee merge request {review.get_attribute('reference')}"
        merge_when_pipeline_succeeds(review, LOG, m_commit)
    return review


def _is_valid_dependency(dependency: Dependency, new_tag: SAHTag, integration_branch: str) -> bool:
    """ Check if this dependency should be updated for the given tag/integration branch. """
    if not dependency.ref or not SAHTag.validate(dependency.ref):
        LOG.warning(f"Skipping {str(dependency)} as it doesn't follow a valid tag")
        return False
    old_tag = SAHTag(dependency.ref)
    if old_tag.get_branch() != new_tag.get_branch():
        LOG.info(f"Skipping {str(dependency)} as it doesn't match {str(new_tag)}")
        return False
    if old_tag >= new_tag:
        LOG.info(f"Skipping {str(dependency)} as it is newer or the same as {str(new_tag)}")
        return False
    if "a" in (old_tag.letter, new_tag.letter):
        LOG.info(f"Skipping {str(dependency)}:{new_tag} as we don't want to integrate"
                 " 'a' based tags")
        return False
    if _dependency_already_prepared(dependency, new_tag, integration_branch):
        LOG.info(f"Skipping {str(dependency)} as it is already present on {integration_branch}")
        return False
    return True


def _get_issues_between(old_tag: SAHTag, new_tag: SAHTag, repo: SAHRepo) -> List[Issue]:
    try:
        return repo.get_issues_between(str(old_tag), str(new_tag),
                                       default_dummy=True, plain_issue=True)
    except GitlabGetError as err:
        if old_tag.get_branch() == "master":
            return repo.get_issues_between(str(old_tag.rebase("master" if not old_tag.base
                                                              else "")),
                                           str(new_tag), default_dummy=True, plain_issue=True)
        raise err




@safe_integration
def repository_update(dependency_group: DependencyGroup,
                      new_tag: SAHTag, new_opensource_tag: str, name: Name) -> Optional[Review]:
    """ Updates a set of dependencies for a single dependant, to a single integration branch.
    The list of dependencies is first thinned out, as dependencies not matching the desired
    new tag should be ignored. If integration isn't needed (newer or the same tag), the dependency
    is also skipped. When integrating to a ConfigRepo, only whitelisted components are allowed.

    When creating the commit/mr, a list of issues will be displayed. This list is compiled based
    on the oldest encountered tag in the integration compared to the new tag.

    If the integration fails due to a conflict, it is retried, up until a maximum number of retries.
    """
    oldest_tag = None
    dep_null = dependency_group.dependencies[0]
    if isinstance(dep_null.dependant, ConfigRepo) and not dep_null.is_whitelisted():
        LOG.info(f"Skipping over {str(dep_null)} as it is not whitelisted")
        return None
    dependencies = []
    for dependency in dependency_group.dependencies:
        if not _is_valid_dependency(dependency, new_tag, dependency_group.strategy.branch):
            continue
        old_tag = SAHTag(dependency.ref)
        dependencies.append((dependency, new_tag.rebase(old_tag.base)))
        LOG.info(f"Updating {str(dependency)}")
        if not oldest_tag or old_tag <= oldest_tag:  # <= is faster than <
            oldest_tag = old_tag
    if not dependencies or not oldest_tag:
        LOG.info("No dependency found to upgrade for group "
                 f"{dependency_group.dependant_name}:{dep_null.dependant_ref}")
        return None

    # Collect issues
    component_repo = repo_create_by_name(name, class_hint=PlainRepo)
    LOG.info(f"Retrieving issues for {str(component_repo)} between"
             f" {str(oldest_tag)} and {str(new_tag)}")
    issues = _get_issues_between(oldest_tag, new_tag, component_repo)

    try:
        dep_null.dependant.create_branch(dependency_group.strategy.branch,
                                         dep_null.dependant_ref)
    except GitlabCreateError:
        pass  # Branch already exists
    workdir, parent_repo = setup_workdir(dep_null, dependency_group.strategy.branch)
    retries = 5
    while retries > 0:
        try:
            # Force clean state
            _reset(parent_repo, dependency_group.strategy.branch)
            # Update remote and sync branches
            updated_files = update_remote(dependencies,
                                          new_opensource_tag,
                                          dependency_group.strategy.branch,
                                          issues, parent_repo, retries)
            break
        except (BadName, ConflictError):
            retries = retries - 1
    if not updated_files:
        LOG.info("No files were updated, no need to continue")
        return None
    # Setup MR if needed/wanted
    merge_strategy = dependency_group.strategy
    review = setup_merge_request(dep_null, new_tag, dependency_group.strategy.branch, issues,
                                 updated_files, merge_strategy, parent_repo)

    return review


def main():
    """ Main entrypoint of sah-component-integrate
    This method will kickstart the integration. For each dependency group (logical set of
    dependencies based on a shared dependant and integration branch), a seperate thread is created
    to perform the integration. The integration should therefor be threadsafe.
    """
    start_time = time.time()

    new_tag = SAHTag(ARGS.tag)
    if ARGS.from_path:
        name = NAME_FACTORY.get_name_for_repo(ARGS.component, git=ARGS.git)
    else:
        name = get_name_for_component(NAME_FACTORY, ARGS.component, git=ARGS.git)

    LOG.info(f"Starting integration of {name.sah_name if name.sah_name else name.openwrt_name} to "
             f"{str(new_tag)}")
    repo = repo_create_by_name(name, haystack=HAY)
    copybara = repo.parse_yml("baf.yml", "copybara", ARGS.tag, None)
    new_opensource_tag = str(new_tag).replace(copybara.get('source_branch',
                                                           'master') + "_v",
                                              copybara.get("destination_branch",
                                                           "main") + "_v").replace("main_v", "v") \
        if copybara else str(new_tag)


    projects = sahlab.list_projects(project_filter=f'{repo.get_haystack_integration_query()} | \
                                                   .[] | select(.ci != false)',
                                    haystack=HAY,
                                    unique=True)

    dependencies = sahlab.find_component(name, projects)
    # Group dependencies by project/ref combination (grouped config commits)
    dependency_groups = group_dependencies(dependencies)
    if not dependency_groups:
        LOG.warning("Component not tracked, no need to integrate!")
        return

    # For each dep
    with ThreadPoolExecutor(max_workers=int(ARGS.thread_factor),
                            thread_name_prefix='repository_update') as executor:
        futures = [executor.submit(repository_update, dependency_group, new_tag, new_opensource_tag,
                                   name) for dependency_group in dependency_groups]

    res = []
    for future in futures:
        res.append(future.result())

    elapsed_time = time.time() - start_time
    LOG.info(f"Finished integration! Integrated {len(list(filter(None, res)))} MR!"
             f" Elapsed time {time.strftime('%Mm%Ss', time.gmtime(elapsed_time))}.")


def run():
    """ Runs the main entrypoint and exits appropriately. """
    main()
    integration_exit()


if __name__ == '__main__':
    run()
