""" Re-usable logging functions """
import logging


def get_logger(verbose: bool = False):
    """ Creates a logger object with custom formatting """
    logger = logging.getLogger()
    log_level = logging.INFO if verbose else logging.WARNING
    logger.setLevel(log_level)

    logger.handlers.clear()
    handler = logging.StreamHandler()
    logger.addHandler(handler)

    formatter = logging.Formatter('%(asctime)s [%(levelname)s] [%(threadName)s] %(message)s')
    handler.setFormatter(formatter)

    return logger
