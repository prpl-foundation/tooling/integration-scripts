""" Sahbot configuration parsing and retrieval """
import asyncio
import re
from dataclasses import dataclass
from typing import List, Optional

from gitlab.exceptions import GitlabGetError, GitlabMRClosedError
from sahlab.repository import (ConfigRepo, Dependency, SAHRepo, WrtConfigRepo,
                               YoctoDistroRepo, PrplConfigRepo)
from sahlab.review import Review

from integration_scripts.utilities import gather_sahbot_yml, parse_sahbot


@dataclass
class MergeStrategy:
    """ Data container to group all strategy information collected from the .sahbot.yml file """
    branch: str
    strategy: str
    auto_merge: bool = False
    auto_label: bool = False


def _determine_merge_strat_meta(dep: Dependency,
                                sahbot_conf: Optional[dict], log) -> MergeStrategy:
    # Sahbot parsing
    if not sahbot_conf:
        return MergeStrategy(f"dev_{dep.dependant_ref}_{dep.name}_upstep", "individual")
    components = parse_sahbot(sahbot_conf, "components", {}, log)
    if dep.name in components:
        match = dep.name
    else:
        match_list = [key for key in components if re.match(f'^{key}$', dep.name)]
        match = match_list[0].replace(".", r'\.') if match_list else "default"

    merge_strategy = parse_sahbot(sahbot_conf, f"components.{match}.merge_strategy",
                                  "individual", log)
    auto_merge = parse_sahbot(sahbot_conf, f"components.{match}.auto_merge", False, log)
    auto_label = parse_sahbot(sahbot_conf, f"components.{match}.auto_label", False, log)

    # MergeStrategy construction
    if merge_strategy.startswith("group_"):
        return MergeStrategy(f"dev_{dep.dependant_ref}_{merge_strategy}_upstep",
                             merge_strategy, auto_merge, auto_label)
    if merge_strategy == "individual":
        sanitized_name = dep.name.replace(':', '_').replace('/', '_')
        return MergeStrategy(f"dev_{dep.dependant_ref}_{sanitized_name}_upstep",
                             merge_strategy, auto_merge, auto_label)
    raise ValueError(f"Unsupported merge strategy for component {dep.name}: {merge_strategy}")


def _determine_merge_strat_config(dep: Dependency,
                                  sahbot_conf: Optional[dict], log) -> MergeStrategy:
    # prpl case
    if re.match(r"^mainline-[0-9]+\.[0-9]+$", dep.dependant_ref):
        return MergeStrategy(f"latest-{dep.dependant_ref[9:]}", "latest")
    source = "s_" + dep.dependant_ref + "_" if dep.dependant_ref != "master" else ""
    if not sahbot_conf:
        # Default
        return MergeStrategy(f"{source}latest", "latest")
    config = dep.found_in.rsplit("/", 1)[0]
    if config.replace('/', '_') not in sahbot_conf.get('configs', {}):
        merge_strategy = parse_sahbot(sahbot_conf, "configs.default.merge_strategy", "latest", log)
    else:
        merge_strategy = parse_sahbot(sahbot_conf,
                                      f"configs.{config.replace('/', '_')}.merge_strategy",
                                      "latest", log)
    if merge_strategy == "latest":
        return MergeStrategy(f"{source}latest", merge_strategy)
    if merge_strategy == "latest_board":
        return MergeStrategy(f"{source}latest_{config.split('/')[-2]}", merge_strategy)
    raise ValueError(f"Unsupported merge strategy for component {config}: {merge_strategy}")


def determine_merge_config_from_dep(dep: Dependency, log) -> MergeStrategy:
    """ Looks up the merge strategy for this dependency for its dependant. This information is
    stored in the .sahbot.yml file in the dependant repository.
    """
    sahbot_conf = gather_sahbot_yml(dep.dependant, dep.dependant_sha)
    if isinstance(dep.dependant, (ConfigRepo, WrtConfigRepo, YoctoDistroRepo, PrplConfigRepo)):
        return _determine_merge_strat_config(dep, sahbot_conf, log)
    return _determine_merge_strat_meta(dep, sahbot_conf, log)


def _determine_merge_strat_config_from_file(updated_file: str,
                                            sahbot_conf: Optional[dict]) -> MergeStrategy:
    """ Determines sahbot configuration based on an updated file.
    Currently only auto_merging is deduced from this.
    """
    if not sahbot_conf:
        # Default
        return MergeStrategy("latest", "latest")
    config = updated_file.rsplit("/", 1)[0]
    if config.replace('/', '_') not in sahbot_conf.get('configs', {}):
        auto_merge = sahbot_conf.get('configs', {}) \
                                .get('default', {}) \
                                .get('auto_merge', False)
    else:
        auto_merge = sahbot_conf.get('configs', {}) \
                                .get(config.replace('/', '_'), {}) \
                                .get('auto_merge', False)
    return MergeStrategy("latest", "latest", auto_merge)


def determine_merge_config_from_repo(repo: SAHRepo, ref: str, log,
                                     updated_files: List[str] = None) -> List[MergeStrategy]:
    """ Looks up the merge strategy for this repository, for the given ref, with the given list
    of updated files. A merge strategy is returned for each updated file, as they can differ.
    """
    sahbot = gather_sahbot_yml(repo, ref)
    if not isinstance(repo, (ConfigRepo, WrtConfigRepo, YoctoDistroRepo, PrplConfigRepo)):
        raise ValueError(f"Unsupported integration target for repo {str(repo)}")
    updated_files = updated_files if updated_files else []
    merge_strategies = []
    for file in updated_files:
        try:
            merge_strategies.append(_determine_merge_strat_config_from_file(file, sahbot))
        except (AttributeError, ValueError) as err:
            log.error("Failed to determine merge strategy for %s, invalid .sahbot.yml?", str(repo))
            log.error(str(err))
            continue
    return merge_strategies


def merge_when_pipeline_succeeds(review: Review, logger, merge_commit: str = None):
    """ Attempts to merge when pipeline succeeds. Creates its own event loop for multithreading
    support. Catches timeouts and Gitlab API failures. """
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    try:
        loop.run_until_complete(review.merge_when_pipeline_succeeds(merge_commit, retry=5))
        logger.info("Successfully set merge_when_pipeline_succeeds for "
                    f"{review.get_attribute('references').get('full')}")
    except TimeoutError as timeout_error:
        logger.error(str(timeout_error))
    except (GitlabGetError, GitlabMRClosedError) as mr_error:
        logger.error("Failed setting merge_when_pipeline_succeeds for "
                     f"{review.get_attribute('references').get('full')}")
        logger.error(str(mr_error))
    finally:
        # Workaround for weird Gitlab bug closing MR
        if review.get_attribute("state") == "closed":
            review.update_attributes(state_event="reopen")
        loop.close()
