FROM registry.gitlab.com/prpl-foundation/tooling/docker/python-toolbox:v1.8.3

ARG VERSION
ARG BUILD_DATE

RUN useradd -ms /bin/bash sahbot

RUN apt-get update && apt-get install -y --no-install-recommends \
    gawk \
    git \
    libncurses5 \
    rsync \
    && rm -rf /var/lib/apt/lists/*

USER sahbot
WORKDIR /home/sahbot
ENV PATH "$PATH:/home/sahbot/.local/bin"
COPY . .

RUN git clone -b v1.32.4 https://gitlab.com/prpl-foundation/tooling/sahlab.git ### sahlab

RUN cd sahlab && \
    version=$(git tag --points-at HEAD | tail -n 1 | sed -n 's/.*\(v[0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/p') && \
    sed -i "s/version='99.99.99'/version='$version'/g" setup.py && \
    python3 -m pip install --upgrade . && \
    cd ../ && \
    rm -rf sahlab

RUN python3 -m pip install --upgrade .

ENV CONTAINER_VERSION ${VERSION}
ENV CONTAINER_BUILD_DATE=${BUILD_DATE}

ENTRYPOINT ["bash"]
